import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {HomeComponent} from './components/home/home.component';

const appRoute = [

    {path: '', component: HomeComponent},
    {path: 'recipe', loadChildren: './components/recipes/recipes.module#RecipesModule'},
    {path: 'shopping-list', loadChildren: './components/shopping-list/shopping.module#ShoppingModule'},

];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoute)
    ],
    exports: [RouterModule],
})
export class AppRouting {

}