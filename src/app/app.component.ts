import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'projectAngular7';
    loadedFeature = 'recipe';



    onNavigate(feature: string) {
        this.loadedFeature = feature;
    }

    ngOnInit(): void {
        firebase.initializeApp({
            apiKey: 'AIzaSyAFCTdb3rcl9goPft1VSnBahu-jlg09sH0',
            authDomain: 'ng-recipe-book-20e41.firebaseapp.com',
        });
    }
}
