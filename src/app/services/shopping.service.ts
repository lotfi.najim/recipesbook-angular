import {EventEmitter, Injectable} from '@angular/core';
import {Ingredient} from '../models/ingredient.model';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ShoppingService {
    ingredientChanges = new Subject<Ingredient[]>();
    startedEdititng = new Subject<number>();


    private ingriedients: Ingredient[] = [
        new Ingredient('Apple', 5),
        new Ingredient('Banana', 15),
    ];

    constructor() {
    }

    getIngredients() {
        return this.ingriedients.slice();
    }

    addNewIngredient(ingredient: Ingredient) {
        this.ingriedients.push(ingredient);
        this.ingredientChanges.next(this.ingriedients.slice());
    }

    addIngredientRecipeToShoppingList(ingredients: Ingredient[]) {
        this.ingriedients.push(...ingredients);
        this.ingredientChanges.next(this.ingriedients.slice());
    }

    getIngredient(index: number) {
        return this.ingriedients[index];
    }

    updateIngredient(index: number, newIngredient: Ingredient) {
        this.ingriedients[index] = newIngredient;
        this.ingredientChanges.next(this.ingriedients.slice());
    }

    getDeleteItem(index: number) {
        this.ingriedients.splice(index, 1);
        this.ingredientChanges.next((this.ingriedients.slice()));
    }
}
