import {EventEmitter, Injectable} from '@angular/core';
import {Recipe} from '../models/recipe.model';
import {Ingredient} from '../models/ingredient.model';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class RecipeService {
    recipedChanged = new Subject<Recipe[]>();
    ingredientChanged = new Subject<Ingredient[]>();

    public recipes: Recipe[] = [
        new Recipe(
            'recipe 12',
            'description 1',
            'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
            [
                new Ingredient('meat', 1),
                new Ingredient('fries', 20),
            ]
        ),
        new Recipe(
            'recipe 21',
            'description 1',
            'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
            [
                new Ingredient('Buns', 2),
                new Ingredient('Meat', 1),

            ]
        )
    ];

    constructor(private http: HttpClient, private auth: AuthService) {
    }

    setRecipes(recipes: Recipe[]) {
        this.recipes = recipes;
        this.recipedChanged.next(this.recipes.slice());
    }

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(id: number) {
        return this.recipes[id];
    }

    updateRecipe(id: number, newRecipe: Recipe) {
        this.recipes[id] = newRecipe;
        this.recipedChanged.next(this.recipes.slice());
    }

    addNewRecipe(newRecipe: Recipe) {
        this.recipes.push(newRecipe);
        this.recipedChanged.next(this.recipes.slice());
    }

    deleteRecipe(id: number) {
        this.recipes.splice(id, 1);
        this.recipedChanged.next(this.recipes.slice());
    }

    saveRecipes() {
        return this.http.put('https://ng-recipe-book-20e41.firebaseio.com/data.json', this.getRecipes());
    }


    fetchRecipes() {
        const token = this.auth.getToken();
        // console.log('fetchRecipes');
        // console.log(token);
        // console.log('fetchRecipes');
        return this.http.get('https://ng-recipe-book-20e41.firebaseio.com/data.json?auth=' + token);
    }
}
