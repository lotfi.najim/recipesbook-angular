import {Directive, ElementRef, Host, HostListener, OnInit, Renderer2} from '@angular/core';

@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective implements OnInit {
    isOpen = false;

    constructor(private elemRef: ElementRef, private render: Renderer2) {
    }

    ngOnInit(): void {
    }

    @HostListener('click') openCloseToggle() {
        this.isOpen = !this.isOpen;
        (this.isOpen) ? this.render.addClass(this.elemRef.nativeElement, 'open')
            : this.render.removeClass(this.elemRef.nativeElement, 'open');

    }


}
