import {Component, OnInit} from '@angular/core';
import {Recipe} from '../../models/recipe.model';
import {RecipeService} from '../../services/recipe.service';
import {Params} from '@angular/router';

@Component({
    selector: 'app-recipes',
    templateUrl: './recipes.component.html',
    styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
    selectedRecipe: Recipe;

    constructor(private recipeService: RecipeService) {
    }

    ngOnInit() {
        // this.recipeService.recipedChanged.subscribe(
        //     (params: Params) => {
        //         // this.selectedRecipe = params;
        //     }
        // );
    }

}
