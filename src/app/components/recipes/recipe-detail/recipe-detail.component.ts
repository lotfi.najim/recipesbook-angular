import {Component, Input, OnInit} from '@angular/core';
import {Recipe} from '../../../models/recipe.model';
import {ShoppingService} from '../../../services/shopping.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {RecipeService} from '../../../services/recipe.service';
import {Ingredient} from '../../../models/ingredient.model';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-recipe-detail',
    templateUrl: './recipe-detail.component.html',
    styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
    recipe: Recipe;
    id: number;

    constructor(
        private shoppingService: ShoppingService,
        private recipeService: RecipeService,
        private route: ActivatedRoute,
        private router: Router,
        private auth: AuthService
    ) {
    }

    ngOnInit() {
        this.route.params.subscribe(
            (params: Params) => {
                this.id = +params['id'];
                this.recipe = this.recipeService.getRecipe(this.id);
            }
        );
    }


    onAddToShoppingList() {
        this.shoppingService.addIngredientRecipeToShoppingList(this.recipe.ingredients);
    }

    onDelete() {
        this.recipeService.deleteRecipe(this.id);
        this.router.navigate(['../'], {relativeTo: this.route});
    }
}
