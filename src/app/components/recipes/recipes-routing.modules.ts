import {NgModule} from '@angular/core';
import {RecipeInfoComponent} from './recipe-info/recipe-info.component';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';
import {RecipesComponent} from './recipes.component';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuardService} from '../../services/authGuard.service';


const recipesRoutes: Routes = [
    {
        path: '', component: RecipesComponent, children: [
            {path: '', component: RecipeInfoComponent},
            {path: 'new', component: RecipeEditComponent , canActivate: [AuthGuardService], },
            {path: ':id', component: RecipeDetailComponent},
            {path: ':id/edit', component: RecipeEditComponent, canActivate: [AuthGuardService], },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(recipesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class RecipesRoutingModules {

}