import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecipeInfoComponent} from './recipe-info/recipe-info.component';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';
import {RecipesComponent} from './recipes.component';
import {RecipeListComponent} from './recipe-list/recipe-list.component';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {RecipeItemComponent} from './recipe-list/recipe-item/recipe-item.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RecipesRoutingModules} from './recipes-routing.modules';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RecipesRoutingModules,
        SharedModule
    ],
    declarations: [
        RecipeInfoComponent,
        RecipeEditComponent,
        RecipesComponent,
        RecipeListComponent,
        RecipeDetailComponent,
        RecipeItemComponent,
    ],

})
export class RecipesModule {
}
