import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {RecipeService} from '../../../services/recipe.service';
import {Recipe} from '../../../models/recipe.model';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Ingredient} from '../../../models/ingredient.model';

@Component({
    selector: 'app-recipe-edit',
    templateUrl: './recipe-edit.component.html',
    styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
    id: number;
    recipe: Recipe;
    editingMode: boolean = false;
    formRecipe: FormGroup;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private recipeServer: RecipeService
    ) {
    }

    ngOnInit() {
        return this.route.params.subscribe(
            (params: Params) => {
                this.id = +params['id'];
                this.recipe = this.recipeServer.getRecipe(this.id);
                this.editingMode = params['id'] != null;
                this.initform();

            }
        );

    }

    private initform() {
        const recipe: Recipe = new Recipe('', '', '', []);
        const recipeIngredient = new FormArray([]);
        if (this.editingMode) {
            const fetchRecipe = this.recipeServer.getRecipe(this.id);
            recipe.name = fetchRecipe.name;
            recipe.description = fetchRecipe.description;
            recipe.imagePath = fetchRecipe.imagePath;
            if (fetchRecipe['ingredients']) {
                for (const ingredient of fetchRecipe['ingredients']) {
                    recipeIngredient.push(
                        new FormGroup({
                            'name': new FormControl(ingredient.name, Validators.required),
                            'amount': new FormControl(ingredient.amount, Validators.required)
                        }));
                }
            }
        }

        this.formRecipe = new FormGroup({
            'name': new FormControl(recipe.name, Validators.required),
            'description': new FormControl(recipe.description, Validators.required),
            'imagePath': new FormControl(recipe.imagePath, Validators.required),
            'ingredients': recipeIngredient
        });
    }


    addNewIngredient() {
        (<FormArray>this.formRecipe.get('ingredients')).push(
            new FormGroup({
                'name': new FormControl(null, Validators.required),
                'amount': new FormControl(null, Validators.required),
            })
        );
    }

    getControls() {
        return (<FormArray>this.formRecipe.get('ingredients')).controls;
    }

    onSubmit() {
        if (this.editingMode) {
            this.recipeServer.updateRecipe(this.id, this.formRecipe.value);
        } else {
            this.recipeServer.addNewRecipe(this.formRecipe.value);
        }

        this.onCancel();
    }

    onCancel() {
        this.router.navigate(['../'], {relativeTo: this.route});
    }

    onDeleteIngredient(indexIngredient: number) {
        (<FormArray>this.formRecipe.get('ingredients')).removeAt(indexIngredient);
    }
}
