import {Component, Input, OnInit} from '@angular/core';
import {Recipe} from '../../../../models/recipe.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-recipe-item',
    templateUrl: './recipe-item.component.html',
    styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
    @Input() recipe: Recipe;
    @Input() index: number;


    constructor(private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
    }



}
