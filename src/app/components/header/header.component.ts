import {Component, EventEmitter, Output} from '@angular/core';
import {RecipeService} from '../../services/recipe.service';
import {Recipe} from '../../models/recipe.model';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})

export class HeaderComponent {
    @Output() featureSelected = new EventEmitter<string>();

    constructor(
        private recipeService: RecipeService,
        public auth: AuthService
    ) {
    }

    onSelect(feature: string) {
        this.featureSelected.emit(feature);
    }

    saveData() {
        this.recipeService.saveRecipes().subscribe(
            (response: Response) => {
                // console.log(response);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    getData() {
        console.log('getData');
        this.recipeService.fetchRecipes()
            .subscribe(
                (response: Response) => {

                    const recipes: Recipe[] = Object.values(response);
                    for (let r of recipes) {
                        if (!r['ingredients']) {
                            r['ingredients'] = [];
                        }
                    }
                    this.recipeService.setRecipes(recipes);
                }
            );
    }

    onLogout() {
        this.auth.logout();
    }
}
