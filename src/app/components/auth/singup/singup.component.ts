import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-singup',
    templateUrl: './singup.component.html',
    styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {
    singin: FormGroup;

    constructor(private  auth: AuthService) {
    }

    ngOnInit() {
        this.singin = new FormGroup({
            'email': new FormControl(
                null,
                Validators.required),
            'password': new FormControl(
                null,
                Validators.required)
            // 'confirm_password': new FormControl(null,
            //     Validators.required,
            // )
        });
    }


    onSubmit() {
        const email = this.singin.get('email').value;
        const password = this.singin.get('password').value;
        this.auth.signupUser(email, password);
    }


    checkPasswordConfirmation(control: AbstractControl): Observable<any> | Promise<any> {

        console.log(this.singin.get('password').value);
        const promis = new Promise<any>((resolve) => {
            setTimeout(() => {
                console.log(control.value);
                if (control.value !== this.singin.get('password').value) {
                    resolve({'passwordNotMatch': true});
                }
                return null;
            }, 2500);
        });
        console.log(this.singin);
        return promis;
    }

}
