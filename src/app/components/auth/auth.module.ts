import {NgModule} from '@angular/core';
import {SinginComponent} from './singin/singin.component';
import {SingupComponent} from './singup/singup.component';
import {CommonModule} from '@angular/common';
import {AuthRoutingModule} from './auth-routing.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        SinginComponent,
        SingupComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        AuthRoutingModule
    ]
})
export class AuthModule {
}
