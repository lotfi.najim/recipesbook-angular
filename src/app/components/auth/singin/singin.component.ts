import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-singin',
    templateUrl: './singin.component.html',
    styleUrls: ['./singin.component.css']
})
export class SinginComponent implements OnInit {
    singin: FormGroup;

    constructor(
        private auth: AuthService,
    ) {
    }

    ngOnInit() {
        this.singin = new FormGroup({
            'email': new FormControl(
                null,
                Validators.required),
            'password': new FormControl(
                null,
                Validators.required)
            // 'confirm_password': new FormControl(null,
            //     Validators.required,
            // )
        });
    }

    onSubmit() {
        const email = this.singin.get('email').value;
        const password = this.singin.get('password').value;
        this.auth.signinUser(email, password);
    }
}
