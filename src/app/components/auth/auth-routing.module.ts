import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SingupComponent} from './singup/singup.component';
import {SinginComponent} from './singin/singin.component';

const authRouting: Routes = [
    {path: 'login', component: SinginComponent},
    {path: 'register', component: SingupComponent},
];

@NgModule({
    imports: [
        RouterModule.forChild(authRouting)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {
}