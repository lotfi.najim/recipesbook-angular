import {Component, OnDestroy, OnInit} from '@angular/core';
import {ShoppingService} from '../../../services/shopping.service';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {Ingredient} from '../../../models/ingredient.model';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-shopping-edit',
    templateUrl: './shopping-edit.component.html',
    styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

    form: FormGroup;
    subscription: Subscription;
    editMode = false;
    editItemIndex: number;
    editedItem: Ingredient;

    constructor(private shoppingListService: ShoppingService) {
    }

    ngOnInit() {
        this.form = new FormGroup({
            'name': new FormControl(null, Validators.required),
            'amount': new FormControl(null, [Validators.required, Validators.pattern('^[1-9]+[0-9]*$')]),
        });

        this.subscription = this.shoppingListService.startedEdititng.subscribe(
            (index: number) => {
                this.editMode = true;
                this.editItemIndex = index;
                this.editedItem = this.shoppingListService.getIngredient(index);
                this.form.setValue({name: this.editedItem.name, amount: this.editedItem.amount});
            }
        );
    }

    onSubmit() {
        const name = this.form.value['name'];
        const amount = this.form.value['amount'];
        const ingredient = new Ingredient(name, amount);
        if (this.editMode) {
            this.shoppingListService.updateIngredient(this.editItemIndex, ingredient);
            this.editMode = false;
        } else {
            this.shoppingListService.addNewIngredient(ingredient);
        }
        this.form.reset();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    onClear() {
        this.form.reset();
        this.editMode = false;
    }

    onDelete() {
        this.shoppingListService.getDeleteItem(this.editItemIndex);
        this.onClear();
    }

}

