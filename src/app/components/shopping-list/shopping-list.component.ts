import {Component, EventEmitter, OnInit} from '@angular/core';
import {Ingredient} from '../../models/ingredient.model';
import {ShoppingService} from '../../services/shopping.service';
import {Recipe} from '../../models/recipe.model';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-shopping-list',
    templateUrl: './shopping-list.component.html',
    styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
    ingriedients: Ingredient[];


    constructor(
        private shoppingListService: ShoppingService,
        private auth: AuthService
    ) {
    }

    ngOnInit() {
        this.ingriedients = this.shoppingListService.getIngredients();
        this.shoppingListService.ingredientChanges
            .subscribe((ingredients: Ingredient[]) => {
                this.ingriedients = ingredients;
            });
    }

    onEditItem(index: number) {
        this.shoppingListService.startedEdititng.next(index);
    }

}
