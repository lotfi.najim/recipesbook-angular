import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShoppingListComponent} from './shopping-list.component';
import {ShoppingEditComponent} from './shopping-edit/shopping-edit.component';

const shoppingRoutes: Routes = [
    {
        path: '', component: ShoppingListComponent, children: [
            {path: ':id/edit', component: ShoppingEditComponent},
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(shoppingRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ShoppingRoutingModule {

}