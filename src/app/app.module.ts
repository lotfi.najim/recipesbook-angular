import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';

import {RecipeService} from './services/recipe.service';
import {ShoppingService} from './services/shopping.service';
import {AppRouting} from './app-routing.modules';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from './shared/shared.module';
import {AuthModule} from './components/auth/auth.module';
import {AuthService} from './services/auth.service';
import {AuthGuardService} from './services/authGuard.service';
import { HomeComponent } from './components/home/home.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
    ],
    imports: [
        BrowserModule,
        AppRouting,
        HttpClientModule,
        AuthModule,
        SharedModule
    ],
    providers: [RecipeService, ShoppingService, AuthService, AuthGuardService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
